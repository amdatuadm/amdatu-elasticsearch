# Amdatu ElasticSearch

OSGi integration of the ElasticSearch Java client. Documentation and examples can be found on the [wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+ElasticSearch)