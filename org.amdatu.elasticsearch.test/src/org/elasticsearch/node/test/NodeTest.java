/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.node.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NodeTest {
	
	private volatile Node m_node;
	
	@Before
	public void setup() throws Exception{
		configure(this)
			.add(createFactoryConfiguration("org.amdatu.elasticsearch.node.factory")
					.set("cluster.name", "amdatu")
					.set("node.name", "amdatunode")
					.set("index.store.type", "ram")
					.set("node.local", "true"))
			.add(createServiceDependency().setService(Node.class).setRequired(true)).setTimeout(10, TimeUnit.SECONDS)
			.apply();
		
			
		insertTweetDocument("amdatu", "Testing ElasticSearch");
		insertTweetDocument("amdatu", "ElasticSearch in OSGi");
		insertTweetDocument("amdatu", "OSGi introduction");
		insertTweetDocument("osgi", "New spec now available");
		insertTweetDocument("osgi", "Blogpost about ElasticSearch in OSGi");
		
		
		m_node.client().admin().indices().refresh(new RefreshRequest("twitter")).actionGet();
	}
	
	@Test
	public void testSearchByUser() throws Exception {

		
		SearchResponse response = m_node.client().prepareSearch("twitter")
				.setQuery(QueryBuilders.matchQuery("user", "amdatu"))
		        .execute()
		        .actionGet();
		
		assertEquals(3L, response.getHits().getTotalHits());
	}
	
	@Test
	public void testSearchByMessage() throws Exception {

		
		SearchResponse response = m_node.client().prepareSearch("twitter")
				.setQuery(QueryBuilders.matchQuery("message", "ElasticSearch"))
		        .execute()
		        .actionGet();
		
		assertEquals(3L, response.getHits().getTotalHits());
	}

	private IndexResponse insertTweetDocument(String user, String message) throws IOException {
		XContentBuilder builder = jsonBuilder()
			    .startObject()
			        .field("user", user)
			        .field("message", message)
			    .endObject();
		
		IndexResponse actionGet = m_node.client().prepareIndex("twitter", "tweet").setSource(builder).execute().actionGet();
		return actionGet;
	}		
	
	@After
	public void cleanup() {
		System.out.println("Cleanup");
		m_node.client().admin().indices().delete(new DeleteIndexRequest("twitter")).actionGet();
		cleanUp(this);
	}
}