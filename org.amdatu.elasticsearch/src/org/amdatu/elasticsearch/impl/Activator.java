/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.elasticsearch.impl;

import java.util.Properties;

import org.amdatu.elasticsearch.client.transport.ElasticSearchTransportClientFactory;
import org.amdatu.elasticsearch.node.ElasticsearchClusterNodeFactory;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager dm) throws Exception {
		Properties clusterNodeFactoryProperties = new Properties();
		clusterNodeFactoryProperties.put(Constants.SERVICE_PID, ElasticsearchClusterNodeFactory.PID);
		dm.add(createComponent().setInterface(ManagedServiceFactory.class.getName(), clusterNodeFactoryProperties).setImplementation(
				ElasticsearchClusterNodeFactory.class)
				.add(createServiceDependency().setService(LogService.class).setRequired(false)));

		Properties transportClientFactoryProperties = new Properties();
		transportClientFactoryProperties.put(Constants.SERVICE_PID, ElasticSearchTransportClientFactory.PID);
		dm.add(createComponent().setInterface(ManagedServiceFactory.class.getName(), transportClientFactoryProperties).setImplementation(
				ElasticSearchTransportClientFactory.class));
	}

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

}
