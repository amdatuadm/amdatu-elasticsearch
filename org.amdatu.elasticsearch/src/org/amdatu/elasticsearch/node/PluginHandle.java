/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.elasticsearch.node;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
* Helper class to extract properly user name, repository name, version and plugin name
* from plugin name given by a user.
*/
public class PluginHandle {

    final String name;
    final String version;
    final String user;
    final String repo;

    PluginHandle(String name, String version, String user, String repo) {
        this.name = name;
        this.version = version;
        this.user = user;
        this.repo = repo;
    }

    List<URL> urls() {
        List<URL> urls = new ArrayList<>();
        if (version != null) {
            // Elasticsearch download service
            addUrl(urls, "http://download.elasticsearch.org/" + user + "/" + repo + "/" + repo + "-" + version + ".zip");
            // Maven central repository
            addUrl(urls, "http://search.maven.org/remotecontent?filepath=" + user.replace('.', '/') + "/" + repo + "/" + version + "/" + repo + "-" + version + ".zip");
            // Sonatype repository
            addUrl(urls, "https://oss.sonatype.org/service/local/repositories/releases/content/" + user.replace('.', '/') + "/" + repo + "/" + version + "/" + repo + "-" + version + ".zip");
            // Github repository
            addUrl(urls, "https://github.com/" + user + "/" + repo + "/archive/" + version + ".zip");
        }
        // Github repository for master branch (assume site)
        addUrl(urls, "https://github.com/" + user + "/" + repo + "/archive/master.zip");
        return urls;
    }

    private static void addUrl(List<URL> urls, String url) {
        try {
            urls.add(new URL(url));
        } catch (MalformedURLException e) {
            // We simply ignore malformed URL
        }
    }

    static PluginHandle parse(String name) {
        String[] elements = name.split("/");
        // We first consider the simplest form: pluginname
        String repo = elements[0];
        String user = null;
        String version = null;

        // We consider the form: username/pluginname
        if (elements.length > 1) {
            user = elements[0];
            repo = elements[1];

            // We consider the form: username/pluginname/version
            if (elements.length > 2) {
                version = elements[2];
            }
        }

        if (repo.startsWith("elasticsearch-")) {
            // remove elasticsearch- prefix
            String endname = repo.substring("elasticsearch-".length());
            return new PluginHandle(endname, version, user, repo);
        }

        if (name.startsWith("es-")) {
            // remove es- prefix
            String endname = repo.substring("es-".length());
            return new PluginHandle(endname, version, user, repo);
        }

        return new PluginHandle(repo, version, user, repo);
    }
}