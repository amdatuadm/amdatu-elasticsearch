package org.amdatu.elasticsearch.client.transport;

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import org.amdatu.elasticsearch.client.SearchClientService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

public class ElasticSearchTransportClientFactory implements ManagedServiceFactory {

	public static final String PID = "org.amdatu.elasticsearch.transportclient.factory";
	
	private volatile DependencyManager m_dependencyManager;

	private final Map<String, Component> components = new HashMap<>();

	@Override
	public String getName() {
		return PID;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
	public void updated(String pid, Dictionary properties)
			throws ConfigurationException {
		deleted(pid);
		
		String clusterName = getStringValue("cluster.name", properties);
		String host = getStringValue("transport.host", properties);
		Integer port = getIntegerValue("transport.port", properties);
		
		ElasticSearchTransportClientService transportClientService = new ElasticSearchTransportClientService(clusterName, host, port);
		
		Component component = m_dependencyManager.createComponent()
				.setInterface(SearchClientService.class.getName(), null)
				.setImplementation(transportClientService);
		
		components.put(pid, component);
		m_dependencyManager.add(component);
	}
	
	private String getStringValue(String key, Dictionary<String, ?> properties) throws ConfigurationException {
	    String value = (String) properties.get(key);
	    if (key == null) {
	        throw new ConfigurationException(key, "property is required.");
	    }
	    return value;
	}
	
	private Integer getIntegerValue(String key, Dictionary<String, ?> properties) throws ConfigurationException {
	    String value = (String) properties.get(key);
	    if (value == null) {
	        throw new ConfigurationException(key, "property is required.");
	    }
	    Integer intValue;
	    try {
	        intValue = Integer.parseInt(value);
	    } catch (NumberFormatException nfe) {
	        throw new ConfigurationException(key, "property is not a valid number.");
	    }
	    return intValue;
	}

	@Override
	public void deleted(String pid) {
		Component component = components.remove(pid);
		if (component != null) {
			m_dependencyManager.remove(component);
		}
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void stop() {
		for (Component component : components.values()) {
			m_dependencyManager.remove(component);
		}
	}	
}
