/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.elasticsearch.client.transport;

import org.amdatu.elasticsearch.client.SearchClientService;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

public class ElasticSearchTransportClientService implements SearchClientService {

	private final String clusterName;
	private final String host;
	private final int port;
	
	public ElasticSearchTransportClientService(String clusterName, String host, int port) { // TODO: Multiple hosts/ports
		this.clusterName = clusterName;
		this.host = host;
		this.port = port;
	}
	
	@Override
	public Client newClient() {
		Settings settings = ImmutableSettings.settingsBuilder()
				.classLoader(Settings.class.getClassLoader())
				.put("cluster.name", clusterName).build();
		TransportClient client = new TransportClient(settings);
		client.addTransportAddress(new InetSocketTransportAddress(host, port));
		return client;
	}

}
