/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.elasticsearch.demo;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.service.command.CommandProcessor;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.search.SearchHit;

@Component(provides=Object.class, properties={
	@Property(name=CommandProcessor.COMMAND_SCOPE, value="search"),
	@Property(name=CommandProcessor.COMMAND_FUNCTION, value="query")
})
public class SearchCommands {
	
	@ServiceDependency
	private volatile Node m_node;
	
	public void query(String q) {
		SearchResponse response = m_node.client().prepareSearch("bank")
				.setQuery(QueryBuilders.multiMatchQuery(q, "firstname", "lastname"))
		        .execute()
		        .actionGet();
		
		for(SearchHit hit : response.getHits()) {
			System.out.println(hit.getSourceAsString());
		}
	}

}
